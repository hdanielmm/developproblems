function alternatingSums(a) {
  const team1 = [];
  const team2 = [];
  let sumTeam1 = 0;
  let sumTeam2 = 0;

  for (let i = 0; i < a.length; i++) {
    i % 2 === 0 ? (sumTeam1 += a[i]) : (sumTeam2 += a[i]);
  }
  team1.push(sumTeam1);
  team2.push(sumTeam2);

  return [sumTeam1, sumTeam2]
}

console.log(alternatingSums([50, 60, 60, 45, 70]));
console.log(alternatingSums([]));

const reverseInParentheses = (inputString) => {

  while(true) {
    const indexFirstClosedParentheses = inputString.indexOf(")");

    if(indexFirstClosedParentheses === -1) break;

    const indexLastOpenedParentheses = inputString.substring(0, indexFirstClosedParentheses).lastIndexOf("(");

    const firstSubstring = inputString.substring(0, indexLastOpenedParentheses);
    const substringReversed = inputString.substring(indexLastOpenedParentheses + 1, indexFirstClosedParentheses).split("").reverse().join("");
    const endSubstring = inputString.substring(indexFirstClosedParentheses + 1, inputString.length);

    inputString = firstSubstring + substringReversed + endSubstring;
  }
  return inputString;
}

console.log(reverseInParentheses("foo(bar(baz))blim"));
console.log(reverseInParentheses("(foo)(bar(baz))(blim)"));
